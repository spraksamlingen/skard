# Skards ordbok

Forsøk på å modellere en eldre ordbok med Ontolex.

## Ontolex og Skard

Jeg har brukt den åpne `ontolex:denotes` til å gi en slags definisjon. Dette fordi databasen for Skard ikke har registrert en definisjon. `LexicalEntry` har koblinger til `lexinfo:partOfSpeach`, `lexinfo:gender` og `lexinfo:number`, men ikke noe som jeg kan forstå passer i `lexinfo:sense`. 

`lexinfo:denotes` er laget for å gi en svak kobling til som man styrer selv. Den kan peke til instanser, klasser eller egenskaper.

## Diagram

Et første utkast ligger på Ontodia: http://www.ontodia.org/diagram?sharedDiagram=134qaikm5322aq314esom7ckh3

![Diagram av Skards ordbok modellert med Ontolex](/skard-arva.png)*[Diagram av Skards ordbok modellert med Ontolex*
